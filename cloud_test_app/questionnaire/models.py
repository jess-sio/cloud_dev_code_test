from django.db import models


class QuestionnaireResponse(models.Model):
    MONTH_OPTIONS = (
        (0, "January"),
        (1, "February"),
        (2, "March"),
        (3, "April"),
        (4, "May"),
        (5, "June"),
        (6, "July"),
        (7, "August"),
        (8, "September"),
        (9, "October"),
        (10, "November"),
        (11, "December")
    )
    DAY_OPTIONS = (
        (0, "Monday"),
        (1, "Tuesday"),
        (2, "Wednesday"),
        (3, "Thursday"),
        (4, "Friday"),
        (5, "Saturday"),
        (6, "Sunday")
    )

    favorite_month = models.IntegerField(choices=MONTH_OPTIONS)
    favorite_day = models.IntegerField(choices=DAY_OPTIONS)
