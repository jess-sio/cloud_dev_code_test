from django import forms

from questionnaire.models import QuestionnaireResponse


class QuestionnaireForm(forms.ModelForm):
    class Meta:
        model = QuestionnaireResponse
        fields = ['favorite_month', 'favorite_day']
        labels = {
            'favorite_month': 'My favourite month.',
            'favorite_day': 'My favourite day of the week.'
        }
