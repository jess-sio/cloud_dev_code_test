from collections import Counter

from django.http import HttpResponseRedirect
from django.shortcuts import render

from questionnaire.forms import QuestionnaireForm
from questionnaire.models import QuestionnaireResponse


def index(request):
    #TODO: make this a real number:
    num_answers = QuestionnaireResponse.objects.count()
    context = {
        'title': "Basic Questions!",
        'num_answers': num_answers,
    }
    return render(request, 'questionnaire/index.html', context)


def questionnaire(request):
    if request.method == 'POST':
        form = QuestionnaireForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')

    form = QuestionnaireForm()
    context = {
        'title': "Basic Questions!",
        'form': form
    }
    return render(request, 'questionnaire/questionnaire.html', context)


def results(request):
    responses = QuestionnaireResponse.objects.all()

    months_names = dict(QuestionnaireResponse.MONTH_OPTIONS)
    days_names = dict(QuestionnaireResponse.DAY_OPTIONS)

    months_counter = Counter(r.favorite_month for r in responses)
    days_counter = Counter(r.favorite_day for r in responses)

    total_responses = QuestionnaireResponse.objects.count()

    months_output = sorted(list(
        (months_names[k], v, '{:.1%}'.format(v / total_responses))
        for k, v
        in months_counter.items()
    ))

    days_output = sorted(list(
        (days_names[k], v, '{:.1%}'.format(v / total_responses))
        for k, v
        in days_counter.items()
    ))

    pairs = [(r.favorite_month, r.favorite_day) for r in responses]
    relations_output = []
    for k, v in months_names.items():
        most_common = Counter(p[1] for p in pairs if p[0] == k).most_common()
        if len(most_common) == 0:
            continue
        day = days_names[most_common[0][0]]
        relations_output.append((v, day))

    context = {
        'months': months_output,
        'days': days_output,
        'relations': relations_output
    }
    return render(request, 'questionnaire/results.html', context)
